package com.gitlab.spring.showcases.cloud.service.customer;

import com.jcabi.aspects.Loggable;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Component
@Path("/customer")
@Api("Provides customer related information.")
public class CustomerResource {
    @Path("{customerId}")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Loggable
    @ApiOperation("Get customer data by customerId")
    public Customer getCustomer(@PathParam("customerId") String customerId) {
        Customer result = new Customer(customerId);
        result.setName("Max Mustermann");
        return result;
    }
}
