package com.gitlab.spring.showcases.cloud.service.contract;

import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@ApplicationPath("/app")
public class BeanResourceConfig extends ResourceConfig implements ApplicationContextAware {
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @PostConstruct
    void register() {
        List resources = getPathAnnotatedBeans();
        resources.forEach(super::register);

        super.register(ApiListingResource.class);
        super.register(SwaggerSerializers.class);
    }

    private List getPathAnnotatedBeans() {
        String[] beanNames = applicationContext.getBeanNamesForAnnotation(Path.class);
        return Arrays.stream(beanNames).map(applicationContext::getBean).collect(Collectors.toList());
    }
}
