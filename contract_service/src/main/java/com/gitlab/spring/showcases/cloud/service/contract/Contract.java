package com.gitlab.spring.showcases.cloud.service.contract;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
public class Contract {
    private String contractId;
    private Date createdAt = new Date();

    public Contract(String contractId) {
        this.contractId = contractId;
    }

    public Contract() {
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
