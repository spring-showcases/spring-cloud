package com.gitlab.spring.showcases.cloud.service.contract;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Component
@Path("/contract")
@Api(value = "Access contract and related information")
public class ContractResource {

    @Path("{contractId}")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Get contract by it's id")
    public Contract getContract(@PathParam("contractId") String contractId) {
        return new Contract(contractId);
    }

}
