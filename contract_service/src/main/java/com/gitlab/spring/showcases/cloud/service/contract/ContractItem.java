package com.gitlab.spring.showcases.cloud.service.contract;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ContractItem {
    private String contractId;
    private String contractItemId;
}
