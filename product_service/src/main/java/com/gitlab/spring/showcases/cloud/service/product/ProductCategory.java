package com.gitlab.spring.showcases.cloud.service.product;

/**
 * @author Stefan Fellinger
 * @since 03.03.17
 */
public class ProductCategory {
    String identifier;
    String description;
}
