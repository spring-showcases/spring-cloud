package com.gitlab.spring.showcases.cloud.service.product;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@NodeEntity
public class Product {

    @GraphId
    private Long id;

    private String productId;

    @Relationship(type="PRODUCT", direction = Relationship.INCOMING)
    Set<ProductPosition> positions;

}
