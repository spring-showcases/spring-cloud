package com.gitlab.spring.showcases.cloud.service.product;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

/**
 * Defines this spring cloud application, enables neo4j
 */
@SpringCloudApplication
@EnableNeo4jRepositories
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args).registerShutdownHook();
    }
}
