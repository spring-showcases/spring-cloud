#!/bin/sh
set -eu
: ${GO_PIPELINE_COUNTER:?}
: ${GPG_KEYNAME:?}
: ${PWD:?}
set -x

RELEASE_VERSION=1.0.0
TAG="v$RELEASE_VERSION"

git commit -m "Release $RELEASE_VERSION"
git tag -u "$GPG_KEYNAME" -m "Jumi $RELEASE_VERSION" "$TAG"
export RELEASE_REVISION=`git rev-parse HEAD`

mkdir build
echo "$RELEASE_VERSION" > build/version
echo "$RELEASE_REVISION" > build/revision

mvn versions:set \
    --batch-mode \
    --errors \
    -DgenerateBackupPoms=false \
    -DnewVersion="$RELEASE_VERSION" \
    --file pom.xml

mvn clean deploy \
    --batch-mode \
    --errors \
    -Psonatype-oss-release \
    -Dgpg.keyname="$GPG_KEYNAME" \
    -Dgpg.passphrase="" \
    -DaltDeploymentRepository="staging::default::file://$PWD/staging"

git commit -m "Prepare for next development iteration"

git init --bare staging.git
git push staging.git "$TAG"
git push staging.git HEAD:master
